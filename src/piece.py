# -*- coding: utf-8 -*-
#
#       piece.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import random
import settings


class Piece(object):
    def __init__(self, shapes):
        if shapes == None:
            self.shapes = [[[0] * settings.CELL_SIZE[0]] *
                           settings.CELL_SIZE[1]]
            return

        for i, shape in enumerate(shapes):
            assert(len(shape) == settings.PIECE_SIZE[1])
            for j, row in enumerate(shape):
                assert(len(row) == settings.PIECE_SIZE[0])
                if i == 0 and j == 0 or j == settings.PIECE_CENTER[1]:
                    for k, square in enumerate(row):
                        if j == settings.PIECE_CENTER[1] and \
                           k == settings.PIECE_CENTER[0]:
                            assert(square == 1)
                        elif i == 0 and j == 0:
                            assert(square == 0)

        self.shapes = shapes

    def total_shapes(self):
        return len(self.shapes)

    def get_table(self, shape_number=0):
        return self.shapes[shape_number]


def random_piece():
    return random.sample(pieces, 1)[0]


pieces = []
for piece in settings.PIECES:
    pieces.append(Piece(piece))
