# -*- coding: utf-8 -*-
#
#       board.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import settings
import utils


class Board(object):
    def __init__(self):
        self.table = []
        for i in xrange(settings.BOARD_SIZE[1]):
            self.table.append([0] * settings.BOARD_SIZE[0])

    def fits(self, piece, x, y, s, commit=False):
        if commit:
            coords = []
        for iy, row in enumerate(piece.get_table(s)):
            for ix, cell in enumerate(row):
                if cell:
                    ax = x + ix - settings.PIECE_CENTER[0]
                    ay = y + iy - settings.PIECE_CENTER[1]
                    if ax < 0 or ay < 0 or ax >= settings.BOARD_SIZE[0] or \
                       ay >= settings.BOARD_SIZE[1] or self.table[ay][ax]:
                        return False
                    if commit:
                        coords.append((ax, ay))
        if commit:
            for coord in coords:
                self.table[coord[1]][coord[0]] = 1
            return True + self.check_rows()
        return True

    def check_rows(self):
        erased_rows = 0
        new_table = []
        for row in self.table:
            if sum(row) != settings.BOARD_SIZE[0]:
                new_table.append(row)
            else:
                erased_rows += 1
        if erased_rows > 0:
            for i in xrange(settings.BOARD_SIZE[1] - len(new_table)):
                new_table.insert(0, [0] * settings.BOARD_SIZE[0])
            self.table = new_table
        return erased_rows

    def draw(self, screen):
        utils.draw_table(screen, settings.MARGIN_LEFT,
                         settings.MARGIN_TOP, self.table)
