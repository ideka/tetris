# -*- coding: utf-8 -*-
#
#       utils.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import pygame
import settings


def draw_table(screen, sx, sy, table, draw_inactive=True):
    for y, row in enumerate(table):
        for x, cell in enumerate(row):
            if not cell and not draw_inactive:
                continue
            draw_cell(screen,
                      sx + x * settings.CELL_SIZE[0],
                      sy + y * settings.CELL_SIZE[1],
                      [settings.CELL_COLOR,
                       settings.ACTIVE_CELL_COLOR][cell],
                      [settings.CELL_BORDER_COLOR,
                       settings.ACTIVE_CELL_BORDER_COLOR][cell])


def draw_cell(screen, x, y, color, border_color):
    if color is not None:
        pygame.draw.rect(screen, color, (x, y,
                                         settings.CELL_SIZE[0] - 1,
                                         settings.CELL_SIZE[1] - 1))
    if border_color is not None:
        pygame.draw.rect(screen, border_color, (x, y,
                                                settings.CELL_SIZE[0] - 1,
                                                settings.CELL_SIZE[1] - 1), 1)
