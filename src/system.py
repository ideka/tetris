# -*- coding: utf-8 -*-
#
#       system.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from game import Game
import settings
import board
import piece
import utils
from input import Keyboard


class System(object):
    def __init__(self, controller):
        self.controller = controller()
        self.board = board.Board()

        self.next_piece = piece.random_piece()
        self.create_piece()

        self.moved = False
        self.rotated = False

    def step(self, step):
        self.moved = False
        self.rotated = False
        self.controller.step(self)
        if step:
            if self.falling:
                self.piece_down(True)
            else:
                self.create_piece()

    def move_piece(self, right):
        assert(not self.moved)
        self.moved = True
        new_x = self.piece_x + (right - (not right))
        if self.board.fits(self.piece, new_x, self.piece_y, self.piece_s):
            self.piece_x = new_x

    def piece_down(self, itself=False):
        if not itself:
            assert(not self.moved)
            self.moved = True
        if self.falling:
            if not itself:
                Game.next_step()
            if self.board.fits(self.piece, self.piece_x,
                               self.piece_y + 1, self.piece_s):
                self.piece_y += 1
            else:
                result = self.board.fits(self.piece, self.piece_x,
                                       self.piece_y, self.piece_s, True)
                assert(result is not False)
                Game.statistic.piece_placed()
                Game.statistic.row_completed(result - 1)
                self.falling = False
        elif not itself:
            Game.next_step(True)

    def rotate_piece(self):
        assert(not self.rotated)
        self.rotated = True
        new_s = (self.piece_s + 1) % self.piece.total_shapes()
        if self.board.fits(self.piece, self.piece_x, self.piece_y, new_s):
            self.piece_s = new_s

    def create_piece(self):
        self.piece = self.next_piece
        self.next_piece = piece.random_piece()
        self.piece_x = (settings.BOARD_SIZE[0] - settings.PIECE_SIZE[0]) \
                       / 2 + settings.PIECE_CENTER[0]
        self.piece_y = 0
        self.piece_s = 0
        if not self.board.fits(self.piece, self.piece_x, self.piece_y,
                               self.piece_s):
            print "You lose!"
            print "Pieces placed: " + str(Game.statistic.placed_pieces)
            print "Completed rows: " + str(Game.statistic.completed_rows)
            print "Time taken: " + Game.statistic.get_current_time()
            Game.end()
        self.falling = True

    def draw(self, screen):
        self.board.draw(screen)
        if self.falling:
            utils.draw_table(screen,
                             settings.MARGIN_LEFT +
                             ((self.piece_x - settings.PIECE_CENTER[0]) *
                              settings.CELL_SIZE[0]),
                             settings.MARGIN_TOP +
                             ((self.piece_y - settings.PIECE_CENTER[1]) *
                              settings.CELL_SIZE[1]),
                             self.piece.get_table(self.piece_s), False)
        utils.draw_table(screen, settings.CELL_SIZE[0] *
                                 (settings.BOARD_SIZE[0] + 1) +
                                 settings.MARGIN_LEFT,
                         settings.MARGIN_TOP, self.next_piece.get_table())
