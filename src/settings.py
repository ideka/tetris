# -*- coding: utf-8 -*-
#
#       settings.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import pygame.locals as lc

# Sizes.
CELL_SIZE = (32, 32)
BOARD_SIZE = (10, 20)

# Margins.
MARGIN_LEFT = CELL_SIZE[0]
MARGIN_TOP = CELL_SIZE[1]
MARGIN_RIGHT = MARGIN_LEFT
MARGIN_BOTTOM = MARGIN_TOP

# Cell colors.
CELL_COLOR = None
CELL_BORDER_COLOR = (0, 0, 0)
ACTIVE_CELL_COLOR = (127, 127, 127)
ACTIVE_CELL_BORDER_COLOR = (0, 0, 0)

# Window appearance.
CAPTION = "Tetris"
BACKGROUND = (255, 255, 255)

# Behaviour.
GAME_SPEED = 60
FRAMES_PER_STEP = 30

# Key config.
KEY_LEFT = lc.K_LEFT
KEY_RIGHT = lc.K_RIGHT
KEY_DOWN = lc.K_DOWN
KEY_ROTATE = lc.K_UP
KEY_HELD_TIMEOUT = 30

# Constants (you probably shouldn't modify these).
PIECE_SIZE = (4, 4)
PIECE_CENTER = (2, 1)

# Pieces
          # O
PIECES = [[[[0, 0, 0, 0],
            [0, 1, 1, 0],
            [0, 1, 1, 0],
            [0, 0, 0, 0]]],

          # I
          [[[0, 0, 0, 0],
            [1, 1, 1, 1],
            [0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0]]],

          # S
          [[[0, 0, 0, 0],
            [0, 0, 1, 1],
            [0, 1, 1, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 0, 0, 1],
            [0, 0, 0, 0]]],

          # Z
          [[[0, 0, 0, 0],
            [0, 1, 1, 0],
            [0, 0, 1, 1],
            [0, 0, 0, 0]],

           [[0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 0, 0, 0]]],

          # L
          [[[0, 0, 0, 0],
            [0, 1, 1, 1],
            [0, 1, 0, 0],
            [0, 0, 0, 0]],

           [[0, 1, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0]],

           [[0, 0, 0, 1],
            [0, 1, 1, 1],
            [0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 0, 0, 0]]],

          # J
          [[[0, 0, 0, 0],
            [0, 1, 1, 1],
            [0, 0, 0, 1],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 0, 0, 0]],

           [[0, 1, 0, 0],
            [0, 1, 1, 1],
            [0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0]]],

          # T
          [[[0, 0, 0, 0],
            [0, 1, 1, 1],
            [0, 0, 1, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 1, 1, 1],
            [0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 0, 0, 0]]]]
