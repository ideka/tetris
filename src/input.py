# -*- coding: utf-8 -*-
#
#       input.py
#
#       Copyright 2011 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import pygame
import pygame.locals as pl


class Input(object):
    quit = False

    @classmethod
    def handle(cls):
        """Handle the intput events.

        If an *UP event and a *DOWN for the same key/button occour in the same
        step, save the *DOWN event in not_taken and later post it to the
        event queue to be used in the next step.

        """
        not_taken = []

        for event in pygame.event.get():
            if event.type == pl.QUIT:
                cls.quit = True

            elif event.type == pl.KEYDOWN:
                Keyboard.keys[event.key] = 1
            elif event.type == pl.KEYUP:
                if not event.key in Keyboard.keys or \
                   Keyboard.keys[event.key] > 1:
                    Keyboard.keys[event.key] = 0
                else:
                    not_taken.append(event)

            elif event.type == pl.MOUSEMOTION:
                Mouse.x, Mouse.y = event.pos
            elif event.type == pl.MOUSEBUTTONDOWN:
                Mouse.buttons[event.button] = 1
            elif event.type == pl.MOUSEBUTTONUP:
                if not event.button in Mouse.buttons or \
                   Mouse.buttons[event.button] > 1:
                    Mouse.buttons[event.button] = 0
                else:
                    not_taken.append(event)

        for event in not_taken:
            pygame.event.post(event)

    @classmethod
    def update(cls):
        cls.quit = False

        for key in Keyboard.keys.keys():
            if Keyboard.keys[key] == 0:
                del Keyboard.keys[key]
            else:
                Keyboard.keys[key] += 1

        for button in Mouse.buttons.keys():
            if Mouse.buttons[button] == 0:
                del Mouse.buttons[button]
            else:
                Mouse.buttons[button] += 1


class Keyboard(object):
    keys = {}

    @classmethod
    def time(cls, key):
        """Return the number of steps "key" has been pressed for.

        If the key has been pressed in the current step, return 1.
        If the key has been released in the current step, return 0.

        """
        try:
            return cls.keys[key]
        except KeyError:
            return None

    @classmethod
    def pressed(cls, key):
        """Return whether "key" has been pressed in the current step."""
        return cls.time(key) == 1

    @classmethod
    def held(cls, key):
        """Return whether "key" is being held."""
        return cls.time(key) > 0

    @classmethod
    def released(cls, key):
        """Return whether "key" has been released in the current step."""
        return cls.time(key) == 0


class Mouse(object):
    buttons = {}
    x = 0
    y = 0

    @classmethod
    def time(cls, button):
        """Return the number of steps "button" has been pressed for.

        If the button has been pressed in the current step, return 1.
        If the button has been released in the current step, return 0.

        """
        try:
            return cls.buttons[button]
        except KeyError:
            return None

    @classmethod
    def pressed(cls, button):
        """Return whether "button" has been pressed in the current step."""
        return cls.time(button) == 1

    @classmethod
    def held(cls, button):
        """Return whether "button" is being held."""
        return cls.time(button) > 0

    @classmethod
    def released(cls, button):
        """Return whether "button" has been released in the current step."""
        return cls.time(button) == 0

    @classmethod
    def wheel_up(cls):
        return cls.released(4)

    @classmethod
    def wheel_down(cls):
        return cls.released(5)
