# -*- coding: utf-8 -*-
#
#       human.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import settings
from input import Keyboard


class Controller(object):
    def step(step, system):
        if Keyboard.pressed(settings.KEY_LEFT) or \
           Keyboard.time(settings.KEY_LEFT) > settings.KEY_HELD_TIMEOUT:
            system.move_piece(False)
        elif Keyboard.pressed(settings.KEY_RIGHT) or \
           Keyboard.time(settings.KEY_RIGHT) > settings.KEY_HELD_TIMEOUT:
            system.move_piece(True)
        elif Keyboard.held(settings.KEY_DOWN):
            system.piece_down()
        if Keyboard.pressed(settings.KEY_ROTATE) or \
           Keyboard.time(settings.KEY_ROTATE) > settings.KEY_HELD_TIMEOUT:
            system.rotate_piece()
