# -*- coding: utf-8 -*-
#
#       cpu.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from copy import deepcopy
import settings
import board
import piece
from input import Keyboard

LEFT = 0
RIGHT = 1
ROTATE = 2
DOWN = 3


class Controller():
    def __init__(self):
        self.plan = None

    def step(self, system):
        if system.falling:
            if self.plan is None:
                self.get_plan(system)
            else:
                todo = self.plan.pop()
                if todo == LEFT:
                    system.move_piece(0)
                elif todo == RIGHT:
                    system.move_piece(1)
                elif todo == ROTATE:
                    system.rotate_piece()
                elif todo == DOWN:
                    system.piece_down()
                if self.plan == []:
                    self.plan = None

    def get_plan(self, system):
        best = None
        best_rating = None
        m_boards = self.meta_boards(system.board, system.piece)
        for i, m_board in enumerate(m_boards):
            #print i, len(m_boards)
            rating = 0
            for mm_board in self.meta_boards(m_board, system.next_piece):
                rating += self.board_rating(mm_board)
                #for piece_ in piece.pieces:
                #    for mmm_board in self.meta_boards(mm_board, piece_):
                #        rating += self.board_rating(mmm_board)
            if rating > best_rating:
                best_rating = rating
                best = m_boards[m_board]
        self.plan = best[::-1]

    def meta_boards(self, board, piece):
        ptable = []
        for i in xrange(settings.BOARD_SIZE[1]):
            ptable.append([])
            for j in xrange(settings.BOARD_SIZE[0]):
                ptable[i].append([])

        for s in xrange(piece.total_shapes()):
            for x in xrange(settings.BOARD_SIZE[0]):
                for y in xrange(settings.BOARD_SIZE[1]):
                    if board.fits(piece, x, y, s):
                        ptable[y][x].append(s)

        def get_path(x, y, s):
            end = (x, y, s)
            nodes = {(x, y, s): 0}
            viewed = []
            stop = False
            while not stop and \
                  ((settings.BOARD_SIZE[0] - settings.PIECE_SIZE[0]) / 2 +
                   settings.PIECE_CENTER[0], 0, 0) not in nodes:
                stop = True
                for (x, y, s), d in nodes.items():
                    if (x, y, s) in viewed:
                        continue
                    stop = False
                    d += 1
                    for xx, yy, ss in [(x + 1, y, s), (x - 1, y, s),
                                       (x, y, (s - 1) %
                                        piece.total_shapes()),
                                       (x, y - 1, s)]:
                        if 0 <= xx < settings.BOARD_SIZE[0] and \
                           0 <= yy < settings.BOARD_SIZE[1] and \
                           ss in ptable[yy][xx] and not (xx, yy, ss) in nodes:
                            nodes[(xx, yy, ss)] = d
                    viewed.append((x, y, s))

            if not ((settings.BOARD_SIZE[0] - settings.PIECE_SIZE[0]) / 2 +
                    settings.PIECE_CENTER[0], 0, 0) in nodes:
                return None

            path = []
            x = (settings.BOARD_SIZE[0] - settings.PIECE_SIZE[0]) / 2 + \
                settings.PIECE_CENTER[0]
            y = 0
            s = 0
            while (x, y, s) != end:
                add = None
                lowest = 0
                for xx, yy, ss, t in [(x - 1, y, s, LEFT),
                                      (x + 1, y, s, RIGHT),
                                      (x, y, (s + 1) % piece.total_shapes(),
                                       ROTATE),
                                      (x, y + 1, s, DOWN)]:
                    if (xx, yy, ss) in nodes and \
                       (add is None or lowest > nodes[(xx, yy, ss)]):
                        x = xx
                        y = yy
                        s = ss
                        add = t
                        lowest = nodes[(x, y, s)]
                        if (x, y, s) == end:
                            path.append(add)
                            return path
                path.append(add)
            return path

        boards = {}
        for y, row in enumerate(ptable):
            for x, cell in enumerate(row):
                for s in cell:
                    if not board.fits(piece, x, y + 1, s):
                        path = get_path(x, y, s)
                        if path is None:
                            continue
                        new = deepcopy(board)
                        new.fits(piece, x, y, s, True)
                        boards[new] = path + [DOWN]

        return boards

    def board_rating(self, board):
        rating = 0
        table = board.table

        for i, row in enumerate(table[::-1]):
            for cell in row:
                if cell:
                    break
            else:
                height = i
                break
        else:
            height = len(table)
        rating -= height

        for x in xrange(settings.BOARD_SIZE[0]):
            first = None
            for y in xrange(settings.BOARD_SIZE[1]):
                if table[y][x]:
                    first = y
                    break
            if first is not None:
                for y in xrange(settings.BOARD_SIZE[1] - 1, -1, -1):
                    if not table[y][x]:
                        rating -= y - first + 1
                        break

        return rating
