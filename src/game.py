# -*- coding: utf-8 -*-
#
#       game.py
#
#       Copyright 2012 Gerardo Marset <gammer1994@gmail.com>
#
#       This file is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 3 of the License, or
#       (at your option) any later version.
#
#       This file is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this file; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

import time
import pygame
from pygame.locals import K_ESCAPE
import settings
from input import Input, Keyboard

from controller.cpu import Controller as CONTROLLER


class Game(object):
    pygame.init()
    clock = pygame.time.Clock()

    screen_size = (settings.CELL_SIZE[0] * (settings.BOARD_SIZE[0] +
                                            settings.PIECE_SIZE[0] + 1) +
                   settings.MARGIN_LEFT + settings.MARGIN_RIGHT,
                   settings.CELL_SIZE[1] * settings.BOARD_SIZE[1] +
                   settings.MARGIN_TOP + settings.MARGIN_BOTTOM)
    display = pygame.display.set_mode(screen_size)
    screen = pygame.Surface(screen_size)

    frame_number = 0

    @classmethod
    def main_loop(cls):
        cls.set_caption(settings.CAPTION)
        cls.system = system.System(CONTROLLER)
        cls.statistic = Statistic()
        while True:
            # Tick...
            cls.clock.tick(settings.GAME_SPEED)
            cls.frame_number = (cls.frame_number + 1) % \
                               settings.FRAMES_PER_STEP

            # Handle keypresses.
            Input.handle()
            if Keyboard.pressed(K_ESCAPE) or Input.quit:
                cls.end()

            # Perform events.
            cls.statistic.update_time()
            cls.system.step(cls.frame_number == 0)

            # Draw stuff.
            if settings.BACKGROUND is not None:
                cls.screen.fill(settings.BACKGROUND)
            cls.system.draw(cls.screen)

            # Blit to the screen.
            cls.display.blit(cls.screen, (0, 0), (0, 0) + cls.screen_size)
            pygame.display.flip()

            # Update keypresses.
            Input.update()

    @classmethod
    def next_step(cls, ready=False):
        cls.frame_number = settings.FRAMES_PER_STEP - ready

    @classmethod
    def get_fps(cls):
        return cls.clock.get_fps()

    @classmethod
    def set_caption(cls, caption):
        pygame.display.set_caption(caption)

    @classmethod
    def end(cls, return_=0):
        print "Leaving..."
        pygame.display.quit()
        exit(return_)


class Statistic(object):
    def __init__(self):
        self.placed_pieces = 0
        self.completed_rows = 0
        self.starting_time = time.time()
        self.total_time = 0

    def piece_placed(self):
        self.placed_pieces += 1

    def row_completed(self, n=1):
        self.completed_rows += n

    def update_time(self):
        self.total_time = time.time() - self.starting_time

    def get_current_time(self):
        hours = str(int(self.total_time / 60 / 60))
        minutes = str(int(self.total_time / 60 % 60))
        seconds = str(self.total_time % 60)
        return hours + ":" + minutes + ":" + seconds


# Prevent circular-importing error.
import system
